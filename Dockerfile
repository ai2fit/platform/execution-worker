ARG JAVA_VERSION=17
ARG GRADLE_VERSION=7.4

FROM gradle:${GRADLE_VERSION}-jdk${JAVA_VERSION} AS build-stage

ARG APP_VERSION
ARG SINGULARITY_ENGINE_VERSION
ENV GRADLE_VERSION_CATALOG_LIBS_SINGULARITY=${SINGULARITY_ENGINE_VERSION}
ENV VERSION=${APP_VERSION}
ENV ORG_GRADLE_PROJECT_version=${APP_VERSION}

# Please note that repository credentials are set in the build-stage.
# It is secure to do so as ARG statements, cause they won't be included in the final image
ARG ORG_GRADLE_PROJECT_mavenUser
ARG ORG_GRADLE_PROJECT_mavenPass

WORKDIR /app
COPY *.gradle ./
COPY src ./src
COPY gradle ./gradle
COPY .git ./.git

RUN set -x && gradle --quiet installDist
RUN ls -la /app/build/install
RUN ls -la /app/build/install/execution-worker

FROM amazoncorretto:${JAVA_VERSION} AS production-stage

ARG UID=1000
ARG GID=1000

RUN yum install shadow-utils -y \
  && groupadd -g "${GID}" singularity \
  && useradd --create-home --no-log-init -u "${UID}" -g "${GID}" singularity \
  && yum clean all

COPY --from=build-stage /app/build/install/execution-worker /opt/execution-worker

ENV APP_HOME=/opt/execution-worker
WORKDIR /opt/execution-worker

RUN chown -R singularity:singularity /opt/execution-worker
RUN mkdir /config && chown -R singularity:singularity /config
RUN mkdir /modules && chown -R singularity:singularity /modules
RUN mkdir /repository && chown -R singularity:singularity /repository
RUN chmod +x /opt/execution-worker/bin/execution-worker

USER singularity

# TODO documentation
# TODO make volume load path configurable? Is there a config option in engine?
RUN mkdir -p ~/.ai2fit
RUN ln -s /modules ~/.ai2fit/modules

CMD ["bin/execution-worker"]
