## Singularity Worker

---

A Singularity Worker for executing AI2Fit pipelines via command line

### Usage

---

Start a given AI2Fit pipeline via the command line:

`execution-worker <pipeline repository path>`

A specified repository configuration of the pipeline can be provided using `-c`:

`execution-worker -c=/path/to/repository_config.json <pipeline repository path>`

`-v` allows overwriting pipeline variables. This flag can be used several times to define multiple pipeline variables at the same time:

`execution-worker -v=variableName1=variableValue1 -v=variableName2=variableValue2 <pipeline repository path>`

### Available command line arguments

The following command line arguments can be used to specify certain pipeline settings and variables

| Argument                       | Description                                                                                                                                    | Usage                        | Default                |
|--------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|------------------------|
| `<pipeline repository path>`   | Provide the repository path of the pipeline to be executed                                                                                     | `<pipeline repository path>` | -                      |
| `-c, --repository-config-path` | Define a path for a specified repository configuration of the AI2Fit pipeline                                                                  | `-c=FILE`                    | repository_config.json |
| `-l, --log-level`              | Define a log level for the root logger                                                                                                         | `-l=LEVEL`                   | UNKNOWN                |
| `-v, --variable`               | Allows overwriting pipeline variables. Note that this flag can be used several times to overwrite multiple pipeline variables at the same time | `-v=KEY=VALUE`               | -                      |                        |
| `-h, --help`                   | Show help message                                                                                                                              | `-h`                         | -                      |
| `-V, --version`                | Print version information                                                                                                                      | `-V`                         | -                      |


### Available env variables

When using the execution worker within docker context you can define options using environment variables without needing
to change the `command`. The environment variables need to follow a specific naming pattern in order to be matched correctly
to the corresponding command line arguments. The naming pattern starts with an `AI2FIT_` prefix followed by the long name version
of the command line argument in uppercase letters with underscored between the individual words. Example: `AI2FIT_LOG_LEVEL`.
Use the following env variables to specify the corresponding command line arguments:

| Env variable                      | Corresponding command line argument |
|-----------------------------------|-------------------------------------|
| `AI2FIT_PIPELINE_PATH`            | `<pipeline repository path>`        |
| `AI2FIT_REPOSITORY_CONFIG_PATH`   | `-c, --repository-config-path`      |
| `AI2FIT_LOG_LEVEL`                | `-l, --log-level`                   |
| `AI2FIT_VARIABLE_<variable name>` | `-v, --variable`                    |


The following example illustrates how these environment variables can be specified in the context of `docker compose`:

```yaml
...
services:
    worker:
        image: registry.gitlab.com/ai2fit/platform/execution-worker:<version_tag>
        environment:
            AI2FIT_LOG_LEVEL: "debug"
            AI2FIT_REPOSITORY_CONFIG_FILE: "/config/repo.config.json"
            AI2FIT_PIPELINE: "//local/Test pipeline"
            AI2FIT_VARIABLE_variableName: "value"
            AI2FIT_VARIABLE_anotherVariableName: 123
```

The command line option `--variable` is special because it can be defined more than once. To define multiple environment variables which
should be mapped to the `--variable` command line option use the `AI2FIT_VARIABLE_(.*)` naming pattern. An `AI2FIT_VARIABLE_variableName: "value"`
environment variable will be handled as `--variable variableName=value`.

Note that the order of defining the variables is as follows: The CLI will first try to use command line arguments that are explicitly defined in the command. If there is no option provided
within the command the CLI will try to find the required value in the environment variables. If the environment variables have
no value for that option or positional parameter, then the default value configured on the option or positional parameter from the CLI is used.
