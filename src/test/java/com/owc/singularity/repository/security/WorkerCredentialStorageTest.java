package com.owc.singularity.repository.security;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;

import org.junit.jupiter.api.Test;

class WorkerCredentialStorageTest {

    @Test
    void retrievePasswordOfDirectUri() {
        try (WorkerCredentialStorage storage = new WorkerCredentialStorage()) {
            // given
            URI uri = URI.create("https://oldworldcomputing.com");

            // when
            storage.store(uri, "user", "password".toCharArray());

            // then
            assertArrayEquals("password".toCharArray(), storage.retrieve(uri, "user"));
            assertArrayEquals("password".toCharArray(), storage.retrieve(URI.create(uri + "/"), "user"));
            assertArrayEquals("password".toCharArray(), storage.retrieve(URI.create(uri + "/?hello=world"), "user"));
            assertArrayEquals("password".toCharArray(), storage.retrieve(URI.create(uri + "/#section?hello=world"), "user"));
            assertNull(storage.retrieve(uri, "another-user"));
            assertNull(storage.retrieve(URI.create("https://gitlab.com"), "user"));
            assertNull(storage.retrieve(URI.create("https://gitlab.com"), "another-user"));
        }
    }

    @Test
    void retrievePasswordOfDirectUriWithPath() {
        try (WorkerCredentialStorage storage = new WorkerCredentialStorage()) {
            // given
            URI uri = URI.create("https://oldworldcomputing.com/path/to/resource");

            // when
            storage.store(uri, "user", "password".toCharArray());

            // then
            assertArrayEquals("password".toCharArray(), storage.retrieve(uri, "user"));
            assertNull(storage.retrieve(uri, "another-user"));
            assertNull(storage.retrieve(URI.create("https://gitlab.com"), "user"));
            assertNull(storage.retrieve(URI.create("https://gitlab.com"), "another-user"));
        }
    }

    @Test
    void retrievePasswordOfSiblingUri() {
        try (WorkerCredentialStorage storage = new WorkerCredentialStorage()) {
            // given
            URI uri = URI.create("https://owc.com/singularity/worker");

            // when
            storage.store(uri, "user", "password".toCharArray());

            // then
            assertArrayEquals("password".toCharArray(), storage.retrieve(uri, "user"));
            // sibling uri
            assertArrayEquals("password".toCharArray(), storage.retrieve(URI.create("https://owc.com/singularity/worker/resource"), "user"));

            assertNull(storage.retrieve(URI.create("https://owc.com/singularity/studio"), "user"));
            assertNull(storage.retrieve(URI.create("https://owc.de/singularity/worker"), "user"));
            assertNull(storage.retrieve(URI.create("ftp://owc.com/singularity/worker"), "user"));
        }
    }
}
