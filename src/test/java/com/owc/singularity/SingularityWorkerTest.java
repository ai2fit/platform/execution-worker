package com.owc.singularity;

import static com.github.stefanbirkner.systemlambda.SystemLambda.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;

import picocli.CommandLine;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;

public class SingularityWorkerTest {

    @TempDir
    File tempDir;

    @Test
    void exitWithErrorWhenRepositoryConfigDoesNotExists() {
        assertEquals(1, new CommandLine(new SingularityWorker()).execute("-c=none-existing-file", "/my/pipeline"));
    }

    @Test
    void exitWithErrorWhenPipelineDoesNotExists() throws Exception {
        File tempJsonConfig = new File(tempDir, "repository_config.json");

        String jsonString = """
                {"mounts":{"":{"mountType":"file","options":{"read_only":"false","base_directory":""" + "\"" + tempDir.toPath() + "\"" + """
                }}}}
                """;
        Files.writeString(tempJsonConfig.toPath(), jsonString);

        assertEquals(2, new CommandLine(new SingularityWorker()).execute("-c=" + tempJsonConfig.toPath(), "/path/does/not/exist"));
    }

    @Test
    void changePredefinedPipelineValuesWithCommandLineValues() throws Exception {

        PropertyService.setParameterValue(EngineProperties.MODULES_ENABLED, String.valueOf(false));

        ClassLoader classLoader = getClass().getClassLoader();
        File examplePipeline = new File(classLoader.getResource("examplePrintToConsolePipeline.head").getFile());

        File targetPipelineDestination = new File(tempDir, "examplePrintToConsolePipeline.head");
        Files.copy(examplePipeline.toPath(), targetPipelineDestination.toPath(), StandardCopyOption.REPLACE_EXISTING);

        File tempJsonConfig = new File(tempDir, "repository_config.json");
        String jsonString = """
                {"mounts":{"":{"mountType":"file","options":{"read_only":"false","base_directory":""" + "\"" + tempDir.toPath().toString().replace('\\', '/')
                + "\"" + """
                        }}}}
                        """;
        Files.writeString(tempJsonConfig.toPath(), jsonString);

        String clValueToCheck = "check_if_analysis_pipeline_value_was_changed";

        int exitCode = catchSystemExit(() -> {
            try (MockedStatic<OperatorService> operatorServiceMock = Mockito.mockStatic(OperatorService.class, InvocationOnMock::callRealMethod)) {
                OperatorDescription operatorDescription = mock(OperatorDescription.class);
                operatorServiceMock.when(() -> OperatorService.getOperatorDescription("core:print_to_console")).thenReturn(operatorDescription);
                operatorServiceMock.when(() -> OperatorService.createOperator(operatorDescription))
                        .thenAnswer(invocation -> new CheckVariableValueOperator("predefined_analysis_pipeline_variable_name", clValueToCheck));
                new CommandLine(new SingularityWorker()).execute("-c=" + tempJsonConfig.toPath(),
                        "-v=predefined_analysis_pipeline_variable_name=" + clValueToCheck, "examplePrintToConsolePipeline");
            }
        });
    }


    @DefinesOperator(module = "test", key = "test", name = "Print to console", group = "group", icon = "")
    public static class CheckVariableValueOperator extends Operator {

        private final String variableName;
        private final String expectedVariableValue;

        public CheckVariableValueOperator(String variableName, String expectedVariableValue) {
            this.variableName = variableName;
            this.expectedVariableValue = expectedVariableValue;
        }

        @Override
        public void doWork() throws OperatorException {
            String actualVariableValue = getPipeline().getVariableHandler().getVariable(variableName);
            assertThat(actualVariableValue, is(equalTo(expectedVariableValue)));
        }
    }
}
