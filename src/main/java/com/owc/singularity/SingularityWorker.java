package com.owc.singularity;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.EngineExecutionMode;
import com.owc.singularity.engine.EngineExitMode;
import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.concurrency.LocalConcurrencyExecutionService;
import com.owc.singularity.engine.event.PausePreventingPipelineLifeCycleEventListener;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.AnalysisPipeline;
import com.owc.singularity.engine.pipeline.OperatorPath;
import com.owc.singularity.engine.pipeline.io.OperatorPathTreePrinter;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.repository.security.WorkerCredentialStorage;
import com.owc.singularity.telemetry.TelemetryConfigurator;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "execution-worker", mixinStandardHelpOptions = true, version = "1.0", description = "Executes a given AI2FIT Pipeline with the specified repository configuration. See the help (-h, --help) to learn more about the available options.", showDefaultValues = true, defaultValueProvider = ExecutionWorkerDefaultProvider.class)
public class SingularityWorker implements Callable<Integer> {

    @Parameters(index = "0", paramLabel = "<pipeline repository path>", description = "The repository path of the pipeline to be executed.")
    private String pipelinePathArg;

    @Option(names = { "-l", "--log-level" }, paramLabel = "LEVEL", description = "The log level for root logger.", defaultValue = "UNKNOWN")
    private String logLevelArg;

    @Option(names = { "-c",
            "--repository-config-path" }, paramLabel = "FILE", description = "The path for the repository config file.", defaultValue = "repository_config.json")
    private String repositoryConfigFilePathArg;

    @Option(names = { "-v",
            "--variable" }, paramLabel = "KEY=VALUE", description = "Allow overwriting a variable value. Multiple usage of this flag can be used to overwrite multiple variables (e.g. -v variable1=someValue -v variable2=value2", split = ",")
    private Map<String, String> variablePipelineValues;

    @Override
    public Integer call() throws Exception {
        long initializationStart = System.nanoTime();
        SingularityEngine.setExecutionMode(EngineExecutionMode.COMMAND_LINE);

        // set log level for root logger to the defined one
        Level configuredLogLevel = Level.toLevel(logLevelArg, LogService.LEVEL_UNKNOWN);
        if (!configuredLogLevel.equals(LogService.LEVEL_UNKNOWN)) {
            LogService.setRootLevel(configuredLogLevel);
        }
        Logger log = LogManager.getLogger(SingularityWorker.class);
        log.info("Starting Worker {} using {})", getApplicationVersion(), System.getProperty("java.vm.name") + " " + System.getProperty("java.vm.version"));
        log.debug("Execution configurations:\nRepository config file={}\nPipeline path={}\nLog level={}\nVariables={}", repositoryConfigFilePathArg,
                pipelinePathArg, logLevelArg, variablePipelineValues == null ? "" : variablePipelineValues);
        TelemetryConfigurator.configureAndRegisterGlobally();
        Path repositoryConfigFilePath = Path.of(repositoryConfigFilePathArg);

        if (!Files.exists(repositoryConfigFilePath)) {
            log.fatal("Repository config file not found at {}", repositoryConfigFilePath.toAbsolutePath());
            return 1;
        }

        PropertyService.setParameterValue(EngineProperties.GENERAL_REPOSITORY_CONFIG_FILE, repositoryConfigFilePath.toAbsolutePath().toString());
        RepositoryManager.addCredentialStorage(new WorkerCredentialStorage());
        SingularityEngine.init(new LocalConcurrencyExecutionService());

        RepositoryPath repositoryPipelinePath = RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(pipelinePathArg);
        if (!Files.exists(repositoryPipelinePath)) {
            log.fatal("Entry not found, {}", pipelinePathArg);
            return 2;
        }
        Entry entry = Entries.getEntry(repositoryPipelinePath);
        if (!entry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader())) {
            log.fatal("Entry not a pipeline, {}", pipelinePathArg);
            return 3;
        }
        AbstractPipeline pipeline = entry.loadData(AbstractPipeline.class, ModuleService.getMajorClassLoader());
        pipeline.setPath(repositoryPipelinePath);

        // disable breakpoints
        pipeline.registerLifeCycleEventListener(new PausePreventingPipelineLifeCycleEventListener());
        log.info("Worker initialization took {}", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - initializationStart)));
        try {
            // creating execution context
            AnalysisPipeline.AnalysisPipelineExecutionContext executionContext = new AnalysisPipeline.AnalysisPipelineExecutionContext();
            executionContext.logLevel = configuredLogLevel;

            // add pipeline values if they were passed via command line
            if (variablePipelineValues != null) {
                executionContext.variableValues.putAll(variablePipelineValues);
            }

            // run pipeline
            pipeline.run(executionContext, true);
        } catch (ProcessStoppedException e) {
            // although no ui is available, the Stop Pipeline Graciously operator will throw a
            // stopped exception.
            Operator stoppedOperator = e.getOperator();
            if (stoppedOperator != null) {
                log.atInfo()
                        .log("Pipeline \"{}\" stopped by operator:\n{}", pipeline.getPath(),
                                formatPipelineToTreePathOf(OperatorPath.of(stoppedOperator), pipeline));
            } else {
                log.info("Pipeline \"{}\" stopped.", pipeline::getName);
            }
        } catch (Throwable e) {
            OperatorPath operatorPath = null;
            if (e instanceof OperatorException operatorException) {
                operatorPath = OperatorPath.fromException(operatorException);
            } else {
                Operator currentOperator = pipeline.getCurrentOperator();
                if (currentOperator != null) {
                    operatorPath = OperatorPath.of(currentOperator);
                }
            }
            if (operatorPath != null) {
                OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(operatorPath, 3);
                pipeline.walk(pathPrinter);
                log.atError()
                        .withThrowable(e)
                        .log("Pipeline \"{}\" finished with error. Here is the path to the failed operator:\n{}", pipeline.getPath(), pathPrinter.getResult());
            } else {
                log.atError().withThrowable(e).log("Pipeline \"{}\" finished with error at an undetermined operator", pipeline.getPath());
            }
            SingularityEngine.stop(EngineExitMode.ERROR);
        }
        // stop
        SingularityEngine.stop(EngineExitMode.NORMAL);

        return 0;
    }

    public static void main(String... args) {
        int exitCode = new CommandLine(new SingularityWorker()).execute(args);
        System.exit(exitCode);
    }

    private String getApplicationVersion() {
        Package applicationPackage = SingularityWorker.class.getPackage();
        if (applicationPackage == null || applicationPackage.getImplementationVersion() == null)
            return "UNKNOWN";
        return applicationPackage.getImplementationVersion();
    }

    private static String formatPipelineToTreePathOf(OperatorPath path, AbstractPipeline pipeline) {
        OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(path, 3);
        pipeline.walk(pathPrinter);
        return pathPrinter.getResult();
    }
}
