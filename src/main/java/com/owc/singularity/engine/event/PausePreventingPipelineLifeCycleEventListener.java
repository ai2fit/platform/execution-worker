package com.owc.singularity.engine.event;

import com.owc.singularity.engine.pipeline.AbstractPipeline;

public class PausePreventingPipelineLifeCycleEventListener implements PipelineLifeCycleEventListener {

    @Override
    public String getIdentifier() {
        return "SingularityWorker-PausePreventing-PipelineLifeCycleEventListener";
    }

    @Override
    public boolean onBeforePipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        return false;
    }
}
