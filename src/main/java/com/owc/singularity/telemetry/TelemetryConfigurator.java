package com.owc.singularity.telemetry;

import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.exporter.otlp.logs.OtlpGrpcLogRecordExporter;
import io.opentelemetry.instrumentation.log4j.appender.v2_17.OpenTelemetryAppender;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.OpenTelemetrySdkBuilder;
import io.opentelemetry.sdk.logs.SdkLoggerProvider;
import io.opentelemetry.sdk.logs.SdkLoggerProviderBuilder;
import io.opentelemetry.sdk.logs.export.BatchLogRecordProcessor;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import io.opentelemetry.sdk.metrics.SdkMeterProviderBuilder;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.SdkTracerProviderBuilder;
import io.opentelemetry.semconv.resource.attributes.ResourceAttributes;

/**
 * A utility class that {@link #configureAndRegisterGlobally() configures and registers} a telemetry
 * SDK globally.
 *
 * @author Hatem Hamad
 */
public class TelemetryConfigurator {

    private TelemetryConfigurator() {}

    /**
     * Initializes a global {@link OpenTelemetry} using default factory to configure the instance.
     */
    public static synchronized void configureAndRegisterGlobally() {
        SystemPropertiesOpenTelemetrySdkFactory sdkFactory = new SystemPropertiesOpenTelemetrySdkFactory();
        OpenTelemetrySdk openTelemetry = sdkFactory.create();
        try {
            if (openTelemetry == null) {
                GlobalOpenTelemetry.set(OpenTelemetry.noop());
                return;
            }
            GlobalOpenTelemetry.set(openTelemetry);
        } catch (IllegalStateException e) {
            // there is already a functional sdk installed
            return;
        }
        if (sdkFactory.isLogEnabled()) {
            registerCustomLogAppender(openTelemetry);
        }
        Runtime.getRuntime().addShutdownHook(new Thread(openTelemetry::close));
    }

    /**
     * Configuration for the sdk and individual providers are parsed from system properties and
     * environment variables.
     */
    private static class SystemPropertiesOpenTelemetrySdkFactory {

        private static final Logger log = LogManager.getLogger(SystemPropertiesOpenTelemetrySdkFactory.class);

        public OpenTelemetrySdk create() {
            if (!isTraceEnabled() && !isMetricEnabled() && !isLogEnabled()) {
                // no telemetry is enabled
                return null;
            }
            OpenTelemetryConfigurationBuilder configurationBuilder = new OpenTelemetryConfigurationBuilder();
            if (isTraceEnabled()) {
                log.trace("OTEL trace is enabled, configuring tracer");
                configureTracer(configurationBuilder.tracerProvider());
            }
            if (isMetricEnabled()) {
                log.trace("OTEL metric is enabled, configuring meter");
                configureMeter(configurationBuilder.meterProvider());
            }
            if (isLogEnabled()) {
                log.trace("OTEL log is enabled, configuring logger");
                configureLogger(configurationBuilder.loggerProvider());
            }
            return configurationBuilder.addResource(getServiceInformationResource()).build();
        }

        public void configureTracer(SdkTracerProviderBuilder sdkTracerProvider) {

        }

        public void configureMeter(SdkMeterProviderBuilder sdkMeterProvider) {

        }

        public void configureLogger(SdkLoggerProviderBuilder sdkLoggerProvider) {
            String endpoint = getProperty("otel.exporter.otlp.logs.endpoint");
            if (endpoint == null)
                endpoint = getProperty("otel.exporter.otlp.endpoint");
            if (endpoint == null)
                throw new IllegalArgumentException(
                        "No valid endpoint definition was provided. Define one in either 'otel.exporter.otlp.logs.endpoint' or 'otel.exporter.otlp.endpoint'.");
            Duration batchAccumulationDuration = Duration.ofMillis(200);
            sdkLoggerProvider.addLogRecordProcessor(BatchLogRecordProcessor.builder(OtlpGrpcLogRecordExporter.builder().setEndpoint(endpoint).build())
                    // batching logs within 200 millis
                    .setScheduleDelay(batchAccumulationDuration)
                    .build());
            log.debug("telemetry log configuration: endpoint={}, batch accumulation={}ms", endpoint, 200);
        }

        public boolean isTraceEnabled() {
            return Boolean.parseBoolean(getProperty("otel.traces.enabled"));
        }

        public boolean isMetricEnabled() {
            return Boolean.parseBoolean(getProperty("otel.metrics.enabled"));
        }

        public boolean isLogEnabled() {
            return Boolean.parseBoolean(getProperty("otel.logs.enabled"));
        }

        private Resource getServiceInformationResource() {
            return Resource.create(Attributes.of(ResourceAttributes.SERVICE_NAME, getProperty("otel.service.name"), ResourceAttributes.SERVICE_NAMESPACE,
                    getProperty("otel.service.namespace"), ResourceAttributes.SERVICE_VERSION, getProperty("otel.service.version")));
        }

        /**
         * Get property value for a key. Given a key "otel.traces.enabled", try to get the value of
         * the system property "otel.traces.enabled", if not set, the value of environment variable
         * "OTEL_TRACES_ENABLED" will be used.
         * 
         * @param key
         *            the property key
         * @return key value or null
         */
        private static String getProperty(String key) {
            String value = System.getProperty(key);
            if (value != null) {
                return value;
            }
            return System.getenv(key.replace('.', '_').toUpperCase());
        }
    }

    private static void registerCustomLogAppender(OpenTelemetry openTelemetry) {
        Appender openTelemetryLogAppender = createOpenTelemetryLogAppender(openTelemetry);
        openTelemetryLogAppender.start();
        LoggerContext context = (LoggerContext) LogManager.getContext(false);
        Configuration configuration = context.getConfiguration();
        configuration.addAppender(openTelemetryLogAppender);
        configuration.getRootLogger().addAppender(openTelemetryLogAppender, null, null);
        Runtime.getRuntime().addShutdownHook(new Thread(openTelemetryLogAppender::stop));
    }

    private static Appender createOpenTelemetryLogAppender(OpenTelemetry openTelemetry) {
        var builder = OpenTelemetryAppender.builder();
        builder.setName("OpenTelemetryAppender");
        return builder.setOpenTelemetry(openTelemetry).build();

    }

    private static class OpenTelemetryConfigurationBuilder {

        private final Set<Resource> additionalResources = new HashSet<>();
        private SdkTracerProviderBuilder tracerProviderBuilder;
        private SdkMeterProviderBuilder meterProviderBuilder;
        private SdkLoggerProviderBuilder loggerProviderBuilder;


        public OpenTelemetryConfigurationBuilder addResource(Resource resource) {
            this.additionalResources.add(resource);
            return this;
        }

        public SdkTracerProviderBuilder tracerProvider() {
            if (tracerProviderBuilder == null) {
                tracerProviderBuilder = SdkTracerProvider.builder();
            }
            return tracerProviderBuilder;
        }

        public SdkMeterProviderBuilder meterProvider() {
            if (meterProviderBuilder == null) {
                meterProviderBuilder = SdkMeterProvider.builder();
            }
            return meterProviderBuilder;
        }

        public SdkLoggerProviderBuilder loggerProvider() {
            if (loggerProviderBuilder == null) {
                loggerProviderBuilder = SdkLoggerProvider.builder();
            }
            return loggerProviderBuilder;
        }

        protected OpenTelemetrySdkBuilder getDefaultOpenTelemetrySdkBuilder() {
            return OpenTelemetrySdk.builder().setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()));
        }

        protected Resource getAllResources() {
            Resource baseResource = Resource.getDefault();
            for (Resource additionalResource : additionalResources) {
                baseResource = baseResource.merge(additionalResource);
            }
            return baseResource;
        }

        public OpenTelemetrySdk build() {
            Resource resource = getAllResources();
            OpenTelemetrySdkBuilder sdkBuilder = getDefaultOpenTelemetrySdkBuilder();
            if (tracerProviderBuilder != null) {
                sdkBuilder.setTracerProvider(tracerProvider().setResource(resource).build());
            }
            if (meterProviderBuilder != null) {
                sdkBuilder.setMeterProvider(meterProvider().setResource(resource).build());
            }
            if (loggerProviderBuilder != null) {
                sdkBuilder.setLoggerProvider(loggerProvider().setResource(resource).build());
            }
            return sdkBuilder.build();
        }
    }
}
