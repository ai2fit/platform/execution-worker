package com.owc.singularity;

import static java.util.stream.Collectors.*;

import java.util.Map;

import picocli.CommandLine;

public class ExecutionWorkerDefaultProvider implements CommandLine.IDefaultValueProvider {

    private final Map<String, String> envVariables = System.getenv();

    @Override
    public String defaultValue(CommandLine.Model.ArgSpec argSpec) {
        if (argSpec.isPositional()) {
            return envVariables.get("AI2FIT_PIPELINE_PATH");
        } else if (argSpec.isOption()) {
            CommandLine.Model.OptionSpec option = (CommandLine.Model.OptionSpec) argSpec;

            String searchKey = "AI2FIT_" + option.longestName().replace("--", "").replace("-", "_").toUpperCase();

            if (!option.isMultiValue()) {
                return envVariables.get(searchKey);
            }
            Map<String, String> variableMap = envVariables.entrySet()
                    .stream()
                    .filter(k -> k.getKey().startsWith("AI2FIT_VARIABLE_"))
                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
            if (variableMap.isEmpty())
                return null;
            return variableMap.entrySet().stream().map(entry -> entry.getKey().replace("AI2FIT_VARIABLE_", "") + "=" + entry.getValue()).collect(joining(","));
        }
        return null;
    }
}
