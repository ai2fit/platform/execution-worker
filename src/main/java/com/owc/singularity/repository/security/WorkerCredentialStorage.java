package com.owc.singularity.repository.security;

import java.net.URI;
import java.util.*;


public class WorkerCredentialStorage implements CredentialStorage<String, char[]> {

    private final TreeMap<String[], Map<String, char[]>> passwords = new TreeMap<>(Arrays::compare);

    @Override
    public boolean store(URI resourceUri, String principal, char[] credential) {
        if (principal == null)
            return false;
        Map<String, char[]> userPasswordMap = passwords.computeIfAbsent(tokenize(resourceUri), key -> new HashMap<>());
        userPasswordMap.put(principal, credential);
        return true;
    }

    @Override
    public char[] retrieve(URI resourceUri, String principal) {
        String[] key = tokenize(resourceUri);
        Map<String, char[]> result = passwords.get(key);
        // if none found for this uri, search for parents
        for (int i = key.length - 1; (result == null || !result.containsKey(principal)) && i > 0; i--) {
            // remove last path element and repeat until
            // we find a parent path credential
            String[] parentKey = new String[i];
            System.arraycopy(key, 0, parentKey, 0, i);
            result = passwords.get(parentKey);
        }
        if (result == null)
            return null;
        return result.get(principal);
    }

    @Override
    public void close() {}

    private String[] tokenize(URI uri) {
        String[] pathElements = Arrays.stream(uri.getPath().split("/")).filter(segment -> !segment.isBlank()).toArray(String[]::new);
        String[] tokens = new String[pathElements.length + 1];
        tokens[0] = uri.getScheme() + "://" + uri.getAuthority();
        System.arraycopy(pathElements, 0, tokens, 1, pathElements.length);
        return tokens;
    }
}
