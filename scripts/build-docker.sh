#!/bin/bash

# EXECUTE THIS FROM THE MAIN DIRECTORY WHERE THE DOCKERFILE RESIDES

GRADLE_PROPERTIES=$(./gradlew properties --console=plain -q)
APP_VERSION=$(echo "${GRADLE_PROPERTIES}" | grep "^version:" | awk '{printf $2}')
MAVEN_REPO_USER=$(echo "${GRADLE_PROPERTIES}" | grep "^mavenUser:" | awk '{printf $2}')
MAVEN_REPO_PASSWORD=$(echo "${GRADLE_PROPERTIES}" | grep "^mavenPass:" | awk '{printf $2}')

echo $APP_VERSION
IMAGE=singularity-engine:${APP_VERSION}

docker build . -t "${IMAGE}" \
  --build-arg APP_VERSION="${APP_VERSION}" \
  --build-arg ORG_GRADLE_PROJECT_mavenUser="${MAVEN_REPO_USER}" \
  --build-arg ORG_GRADLE_PROJECT_mavenPass="${MAVEN_REPO_PASSWORD}"
