<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues page
and verify the issue you're about to submit isn't a duplicate.
--->

## The problem

(Summarize the bug encountered concisely.)

## Steps to reproduce

### Specifications

* Version: (what version of the software this bug was observed on)
* Branch: (what branch the version is from)
* OS/Platform: (what operating system or the platform (docker/k8s) was the software running on)

<!--- Optional
* CPU Architecture:
* Memory
* GPU Memory:
--->

(Provide steps how to reproduce this bug)

1. Open …
2. Click on …
3. Select …
4. …

## *Actual* behavior

(Describe the current behavior of the program)

## *Expected* behavior

(Describe the expected behavior of the program)

## Log / Stacktrace

<!---

Paste any relevant logs - please use code blocks (```) to format console output,
short logs, and code as it's hard to read otherwise.

If you will paste the full log, please insert the log inside a \<details\> tag, since it make the content collapsable.

<details>
<summary>Expand for server.log output related to the bug</summary>

<pre>
2020-08-19 11:21:47,656 WARNING [de.rapidanalytics.entity.RepositoryException] (http-/0.0.0.0:8080-38) Sending internal server error 500: Error executing process /library/WebApp/processes/static for service static: The repository location 'com.rapidminer.repository.RepositoryException: Requested repository Web Apps Static Content does not exist.' is malformed.: de.rapidanalytics.ejb.service.ServiceDataSourceException: Error executing process /library/WebApp/processes/static for service static: The repository location 'com.rapidminer.repository.RepositoryException: Requested repository Web Apps Static Content does not exist.' is malformed.
at de.rapidanalytics.web.tools.ProcessServiceDataSource.createExampleSet(ProcessServiceDataSource.java:232) [classes:]
at de.rapidanalytics.web.tools.ExportedProcessDelegate.runService(ExportedProcessDelegate.java:131) [classes:]
at de.rapidanalytics.web.tools.ExportedProcessDelegate.runService(ExportedProcessDelegate.java:75) [classes:]
at de.rapidanalytics.web.servlet.ExportedProcessServlet.process(ExportedProcessServlet.java:228) [classes:]
at de.rapidanalytics.web.servlet.ExportedProcessServlet.doGet(ExportedProcessServlet.java:107) [classes:]
</pre>
</details>

--->

<details><summary>Click to see log/stacktrace</summary>

<pre>
!! Paste your log/stacktrace here !!
</pre>

</details>

<!-- Optional
## Screenshots

If relevant, provide some additional screenshots of the problem here
-->

/label ~bug ~"needs approval"
